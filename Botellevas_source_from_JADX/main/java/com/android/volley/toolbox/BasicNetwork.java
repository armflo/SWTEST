package main.java.com.android.volley.toolbox;

import android.os.SystemClock;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.Map;
import java.util.TreeMap;
import main.java.com.android.volley.Cache.Entry;
import main.java.com.android.volley.Network;
import main.java.com.android.volley.Request;
import main.java.com.android.volley.RetryPolicy;
import main.java.com.android.volley.ServerError;
import main.java.com.android.volley.VolleyError;
import main.java.com.android.volley.VolleyLog;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.impl.cookie.DateUtils;

public class BasicNetwork implements Network {
    protected static final boolean DEBUG = VolleyLog.DEBUG;
    private static int DEFAULT_POOL_SIZE = 4096;
    private static int SLOW_REQUEST_THRESHOLD_MS = 3000;
    protected final HttpStack mHttpStack;
    protected final ByteArrayPool mPool;

    public BasicNetwork(HttpStack httpStack) {
        this(httpStack, new ByteArrayPool(DEFAULT_POOL_SIZE));
    }

    public BasicNetwork(HttpStack httpStack, ByteArrayPool pool) {
        this.mHttpStack = httpStack;
        this.mPool = pool;
    }

    /* JADX WARNING: inconsistent code. */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public main.java.com.android.volley.NetworkResponse performRequest(main.java.com.android.volley.Request<?> r27) throws main.java.com.android.volley.VolleyError {
        /*
        r26 = this;
        r23 = android.os.SystemClock.elapsedRealtime();
    L_0x0004:
        r22 = 0;
        r25 = 0;
        r5 = java.util.Collections.emptyMap();
        r21 = new java.util.HashMap;	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r21.<init>();	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r2 = r27.getCacheEntry();	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r0 = r26;
        r1 = r21;
        r0.addCacheHeaders(r1, r2);	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r0 = r26;
        r2 = r0.mHttpStack;	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r0 = r27;
        r1 = r21;
        r22 = r2.performRequest(r0, r1);	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r11 = r22.getStatusLine();	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r13 = r11.getStatusCode();	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r2 = r22.getAllHeaders();	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r5 = convertHeaders(r2);	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r2 = 304; // 0x130 float:4.26E-43 double:1.5E-321;
        if (r13 != r2) goto L_0x0075;
    L_0x003c:
        r20 = r27.getCacheEntry();	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        if (r20 != 0) goto L_0x0054;
    L_0x0042:
        r2 = new main.java.com.android.volley.NetworkResponse;	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r3 = 304; // 0x130 float:4.26E-43 double:1.5E-321;
        r4 = 0;
        r6 = 1;
        r14 = android.os.SystemClock.elapsedRealtime();	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r7 = r14 - r23;
        r2.<init>(r3, r4, r5, r6, r7);	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r10 = r25;
    L_0x0053:
        return r2;
    L_0x0054:
        r0 = r20;
        r2 = r0.responseHeaders;	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r2.putAll(r5);	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r6 = new main.java.com.android.volley.NetworkResponse;	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r7 = 304; // 0x130 float:4.26E-43 double:1.5E-321;
        r0 = r20;
        r8 = r0.data;	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r0 = r20;
        r9 = r0.responseHeaders;	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r10 = 1;
        r2 = android.os.SystemClock.elapsedRealtime();	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r11 = r2 - r23;
        r6.<init>(r7, r8, r9, r10, r11);	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r10 = r25;
        r2 = r6;
        goto L_0x0053;
    L_0x0075:
        r2 = r22.getEntity();	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        if (r2 == 0) goto L_0x00af;
    L_0x007b:
        r2 = r22.getEntity();	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        r0 = r26;
        r10 = r0.entityToBytes(r2);	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
    L_0x0085:
        r2 = android.os.SystemClock.elapsedRealtime();	 Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x018f, MalformedURLException -> 0x018c, IOException -> 0x0189 }
        r7 = r2 - r23;
        r6 = r26;
        r9 = r27;
        r6.logSlowRequests(r7, r9, r10, r11);	 Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x018f, MalformedURLException -> 0x018c, IOException -> 0x0189 }
        r2 = 200; // 0xc8 float:2.8E-43 double:9.9E-322;
        if (r13 < r2) goto L_0x009a;
    L_0x0096:
        r2 = 299; // 0x12b float:4.19E-43 double:1.477E-321;
        if (r13 <= r2) goto L_0x00b3;
    L_0x009a:
        r2 = new java.io.IOException;	 Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x018f, MalformedURLException -> 0x018c, IOException -> 0x0189 }
        r2.<init>();	 Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x018f, MalformedURLException -> 0x018c, IOException -> 0x0189 }
        throw r2;	 Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x018f, MalformedURLException -> 0x018c, IOException -> 0x0189 }
    L_0x00a0:
        r19 = move-exception;
    L_0x00a1:
        r2 = "socket";
        r3 = new main.java.com.android.volley.TimeoutError;
        r3.<init>();
        r0 = r27;
        attemptRetryOnException(r2, r0, r3);
        goto L_0x0004;
    L_0x00af:
        r2 = 0;
        r10 = new byte[r2];	 Catch:{ SocketTimeoutException -> 0x0192, ConnectTimeoutException -> 0x00c4, MalformedURLException -> 0x00d5, IOException -> 0x00f3 }
        goto L_0x0085;
    L_0x00b3:
        r12 = new main.java.com.android.volley.NetworkResponse;	 Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x018f, MalformedURLException -> 0x018c, IOException -> 0x0189 }
        r16 = 0;
        r2 = android.os.SystemClock.elapsedRealtime();	 Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x018f, MalformedURLException -> 0x018c, IOException -> 0x0189 }
        r17 = r2 - r23;
        r14 = r10;
        r15 = r5;
        r12.<init>(r13, r14, r15, r16, r17);	 Catch:{ SocketTimeoutException -> 0x00a0, ConnectTimeoutException -> 0x018f, MalformedURLException -> 0x018c, IOException -> 0x0189 }
        r2 = r12;
        goto L_0x0053;
    L_0x00c4:
        r19 = move-exception;
        r10 = r25;
    L_0x00c7:
        r2 = "connection";
        r3 = new main.java.com.android.volley.TimeoutError;
        r3.<init>();
        r0 = r27;
        attemptRetryOnException(r2, r0, r3);
        goto L_0x0004;
    L_0x00d5:
        r19 = move-exception;
        r10 = r25;
    L_0x00d8:
        r2 = new java.lang.RuntimeException;
        r3 = new java.lang.StringBuilder;
        r4 = "Bad URL ";
        r3.<init>(r4);
        r4 = r27.getUrl();
        r3 = r3.append(r4);
        r3 = r3.toString();
        r0 = r19;
        r2.<init>(r3, r0);
        throw r2;
    L_0x00f3:
        r19 = move-exception;
        r10 = r25;
    L_0x00f6:
        if (r22 == 0) goto L_0x013d;
    L_0x00f8:
        r2 = r22.getStatusLine();
        r13 = r2.getStatusCode();
        r2 = "Unexpected response code %d for %s";
        r3 = 2;
        r3 = new java.lang.Object[r3];
        r4 = 0;
        r6 = java.lang.Integer.valueOf(r13);
        r3[r4] = r6;
        r4 = 1;
        r6 = r27.getUrl();
        r3[r4] = r6;
        main.java.com.android.volley.VolleyLog.m7e(r2, r3);
        if (r10 == 0) goto L_0x017b;
    L_0x0118:
        r12 = new main.java.com.android.volley.NetworkResponse;
        r16 = 0;
        r2 = android.os.SystemClock.elapsedRealtime();
        r17 = r2 - r23;
        r14 = r10;
        r15 = r5;
        r12.<init>(r13, r14, r15, r16, r17);
        r2 = 401; // 0x191 float:5.62E-43 double:1.98E-321;
        if (r13 == r2) goto L_0x012f;
    L_0x012b:
        r2 = 403; // 0x193 float:5.65E-43 double:1.99E-321;
        if (r13 != r2) goto L_0x0145;
    L_0x012f:
        r2 = "auth";
        r3 = new main.java.com.android.volley.AuthFailureError;
        r3.<init>(r12);
        r0 = r27;
        attemptRetryOnException(r2, r0, r3);
        goto L_0x0004;
    L_0x013d:
        r2 = new main.java.com.android.volley.NoConnectionError;
        r0 = r19;
        r2.<init>(r0);
        throw r2;
    L_0x0145:
        r2 = 400; // 0x190 float:5.6E-43 double:1.976E-321;
        if (r13 < r2) goto L_0x0153;
    L_0x0149:
        r2 = 499; // 0x1f3 float:6.99E-43 double:2.465E-321;
        if (r13 > r2) goto L_0x0153;
    L_0x014d:
        r2 = new main.java.com.android.volley.ClientError;
        r2.<init>(r12);
        throw r2;
    L_0x0153:
        r2 = 500; // 0x1f4 float:7.0E-43 double:2.47E-321;
        if (r13 < r2) goto L_0x0175;
    L_0x0157:
        r2 = 599; // 0x257 float:8.4E-43 double:2.96E-321;
        if (r13 > r2) goto L_0x0175;
    L_0x015b:
        r2 = r27.shouldRetryServerErrors();
        if (r2 == 0) goto L_0x016f;
    L_0x0161:
        r2 = "server";
        r3 = new main.java.com.android.volley.ServerError;
        r3.<init>(r12);
        r0 = r27;
        attemptRetryOnException(r2, r0, r3);
        goto L_0x0004;
    L_0x016f:
        r2 = new main.java.com.android.volley.ServerError;
        r2.<init>(r12);
        throw r2;
    L_0x0175:
        r2 = new main.java.com.android.volley.ServerError;
        r2.<init>(r12);
        throw r2;
    L_0x017b:
        r2 = "network";
        r3 = new main.java.com.android.volley.NetworkError;
        r3.<init>();
        r0 = r27;
        attemptRetryOnException(r2, r0, r3);
        goto L_0x0004;
    L_0x0189:
        r19 = move-exception;
        goto L_0x00f6;
    L_0x018c:
        r19 = move-exception;
        goto L_0x00d8;
    L_0x018f:
        r19 = move-exception;
        goto L_0x00c7;
    L_0x0192:
        r19 = move-exception;
        r10 = r25;
        goto L_0x00a1;
        */
        throw new UnsupportedOperationException("Method not decompiled: main.java.com.android.volley.toolbox.BasicNetwork.performRequest(main.java.com.android.volley.Request):main.java.com.android.volley.NetworkResponse");
    }

    private void logSlowRequests(long requestLifetime, Request<?> request, byte[] responseContents, StatusLine statusLine) {
        if (DEBUG || requestLifetime > ((long) SLOW_REQUEST_THRESHOLD_MS)) {
            String str = "HTTP response for request=<%s> [lifetime=%d], [size=%s], [rc=%d], [retryCount=%s]";
            Object[] objArr = new Object[5];
            objArr[0] = request;
            objArr[1] = Long.valueOf(requestLifetime);
            objArr[2] = responseContents != null ? Integer.valueOf(responseContents.length) : "null";
            objArr[3] = Integer.valueOf(statusLine.getStatusCode());
            objArr[4] = Integer.valueOf(request.getRetryPolicy().getCurrentRetryCount());
            VolleyLog.m6d(str, objArr);
        }
    }

    private static void attemptRetryOnException(String logPrefix, Request<?> request, VolleyError exception) throws VolleyError {
        RetryPolicy retryPolicy = request.getRetryPolicy();
        int oldTimeout = request.getTimeoutMs();
        try {
            retryPolicy.retry(exception);
            request.addMarker(String.format("%s-retry [timeout=%s]", new Object[]{logPrefix, Integer.valueOf(oldTimeout)}));
        } catch (VolleyError e) {
            request.addMarker(String.format("%s-timeout-giveup [timeout=%s]", new Object[]{logPrefix, Integer.valueOf(oldTimeout)}));
            throw e;
        }
    }

    private void addCacheHeaders(Map<String, String> headers, Entry entry) {
        if (entry != null) {
            if (entry.etag != null) {
                headers.put("If-None-Match", entry.etag);
            }
            if (entry.lastModified > 0) {
                headers.put("If-Modified-Since", DateUtils.formatDate(new Date(entry.lastModified)));
            }
        }
    }

    protected void logError(String what, String url, long start) {
        long now = SystemClock.elapsedRealtime();
        VolleyLog.m9v("HTTP ERROR(%s) %d ms to fetch %s", what, Long.valueOf(now - start), url);
    }

    private byte[] entityToBytes(HttpEntity entity) throws IOException, ServerError {
        PoolingByteArrayOutputStream bytes = new PoolingByteArrayOutputStream(this.mPool, (int) entity.getContentLength());
        byte[] buffer = null;
        try {
            InputStream in = entity.getContent();
            if (in == null) {
                throw new ServerError();
            }
            buffer = this.mPool.getBuf(1024);
            while (true) {
                int count = in.read(buffer);
                if (count == -1) {
                    break;
                }
                bytes.write(buffer, 0, count);
            }
            byte[] toByteArray = bytes.toByteArray();
            return toByteArray;
        } finally {
            try {
                entity.consumeContent();
            } catch (IOException e) {
                VolleyLog.m9v("Error occured when calling consumingContent", new Object[0]);
            }
            this.mPool.returnBuf(buffer);
            bytes.close();
        }
    }

    protected static Map<String, String> convertHeaders(Header[] headers) {
        Map<String, String> result = new TreeMap(String.CASE_INSENSITIVE_ORDER);
        for (int i = 0; i < headers.length; i++) {
            result.put(headers[i].getName(), headers[i].getValue());
        }
        return result;
    }
}
