package main.java.com.android.volley.toolbox;

import main.java.com.android.volley.AuthFailureError;

public interface Authenticator {
    String getAuthToken() throws AuthFailureError;

    void invalidateAuthToken(String str);
}
