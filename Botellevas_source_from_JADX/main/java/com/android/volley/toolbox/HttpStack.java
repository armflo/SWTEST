package main.java.com.android.volley.toolbox;

import java.io.IOException;
import java.util.Map;
import main.java.com.android.volley.AuthFailureError;
import main.java.com.android.volley.Request;
import org.apache.http.HttpResponse;

public interface HttpStack {
    HttpResponse performRequest(Request<?> request, Map<String, String> map) throws IOException, AuthFailureError;
}
