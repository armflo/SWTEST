package com.botellevasaqp.botellevas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Paso1 extends Activity {
    Bundle f1b;
    private String[] bebidas = new String[]{"Cerveza", "Cremas", "Espumante", "Ginger", "Licores", "Pisco", "Ron", "Vino", "Vodka", "Wisky"};
    String dni;
    String enviar;
    private ListView lv1;
    String trago;

    class C00991 implements OnItemClickListener {
        C00991() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int posicion, long arg3) {
            Paso1.this.enviar = Integer.toString(posicion);
            Paso1.this.trago = Paso1.this.bebidas[posicion];
            Paso1.this.lanzar(v);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.f1b = getIntent().getExtras();
        this.dni = this.f1b.getString("enviard");
        setContentView(C0102R.layout.paso1);
        this.lv1 = (ListView) findViewById(C0102R.id.lv2);
        this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.bebidas));
        this.lv1.setOnItemClickListener(new C00991());
    }

    public void lanzar(View view) {
        Intent i = new Intent(this, Paso2.class);
        i.putExtra("posi", this.enviar);
        i.putExtra("dni2", this.dni);
        i.putExtra("bebida", this.trago);
        startActivity(i);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(C0102R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0102R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
