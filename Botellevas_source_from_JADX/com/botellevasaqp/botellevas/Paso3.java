package com.botellevasaqp.botellevas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.HashMap;
import java.util.Map;
import main.java.com.android.volley.AuthFailureError;
import main.java.com.android.volley.RequestQueue;
import main.java.com.android.volley.Response.ErrorListener;
import main.java.com.android.volley.Response.Listener;
import main.java.com.android.volley.VolleyError;
import main.java.com.android.volley.toolbox.StringRequest;
import main.java.com.android.volley.toolbox.Volley;

public class Paso3 extends Activity {
    private static final String URL = "http://markeproyect.esy.es/student.php";
    Bundle f4b;
    String bebidatipo;
    private EditText cant;
    String cantidad;
    String dni;
    private Button enviarde;
    String f5p;
    String p1;
    private TextView precioto;
    private RequestQueue requestQueue;
    String respuesta;
    private TextView tv1;

    class C01011 implements OnClickListener {

        class C01371 implements Listener<String> {
            C01371() {
            }

            public void onResponse(String response) {
            }
        }

        class C01382 implements ErrorListener {
            C01382() {
            }

            public void onErrorResponse(VolleyError error) {
            }
        }

        C01011() {
        }

        public void onClick(View v) {
            Paso3.this.requestQueue.add(new StringRequest(1, Paso3.URL, new C01371(), new C01382()) {
                public Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> parameters = new HashMap();
                    parameters.put("dni", Paso3.this.dni);
                    parameters.put("bebidatipo", Paso3.this.bebidatipo);
                    parameters.put("bebida", Paso3.this.f5p);
                    parameters.put("cantidad", Paso3.this.cantidad);
                    parameters.put("precio", Paso3.this.p1);
                    parameters.put("preciototal", Paso3.this.respuesta);
                    return parameters;
                }
            });
            Paso3.this.mostrar(v);
            Paso3.this.nuevo(v);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0102R.layout.paso3);
        this.f4b = getIntent().getExtras();
        this.f5p = this.f4b.getString("bebid");
        this.dni = this.f4b.getString("dni3");
        this.bebidatipo = this.f4b.getString("tipobebida");
        this.tv1 = (TextView) findViewById(C0102R.id.textView1);
        this.precioto = (TextView) findViewById(C0102R.id.tvtotal);
        this.cant = (EditText) findViewById(C0102R.id.etcantidad1);
        this.enviarde = (Button) findViewById(C0102R.id.enviar);
        this.tv1.setText(this.f5p);
        this.respuesta = "";
        this.requestQueue = Volley.newRequestQueue(getApplicationContext());
        this.enviarde.setEnabled(false);
        this.enviarde.setOnClickListener(new C01011());
    }

    public void mostrar(View v) {
        Toast.makeText(this, "Se ah enviado el pedido", 0).show();
    }

    public void calcular(View v) {
        this.p1 = this.f4b.getString("preciobe");
        this.cantidad = this.cant.getText().toString();
        this.respuesta = String.valueOf(Double.parseDouble(this.p1) * ((double) Integer.parseInt(this.cantidad)));
        this.precioto.setText("$/" + this.respuesta + "0");
        this.enviarde.setEnabled(true);
    }

    public void nuevo(View v) {
        startActivity(new Intent(getBaseContext(), Paso1.class).addFlags(603979776));
        finish();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(C0102R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0102R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
