package com.botellevasaqp.botellevas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.util.HashMap;
import java.util.Map;
import main.java.com.android.volley.AuthFailureError;
import main.java.com.android.volley.RequestQueue;
import main.java.com.android.volley.Response.ErrorListener;
import main.java.com.android.volley.Response.Listener;
import main.java.com.android.volley.VolleyError;
import main.java.com.android.volley.toolbox.StringRequest;
import main.java.com.android.volley.toolbox.Volley;

public class Datos extends Activity {
    private static final String URL = "http://markeproyect.esy.es/insertStudent.php";
    Bundle f0b;
    private EditText correoa;
    private TextView cost;
    String dinero;
    private EditText direcciona;
    String dni;
    private EditText dnia;
    private Button envio;
    private EditText nombrea;
    private RequestQueue requestQueue;

    class C00971 implements OnClickListener {

        class C01351 implements Listener<String> {
            C01351() {
            }

            public void onResponse(String response) {
            }
        }

        class C01362 implements ErrorListener {
            C01362() {
            }

            public void onErrorResponse(VolleyError error) {
            }
        }

        C00971() {
        }

        public void onClick(View v) {
            if (Datos.this.dnia.getText().toString().equals("") || Datos.this.nombrea.getText().toString().equals("") || Datos.this.direcciona.getText().toString().equals("") || Datos.this.correoa.getText().toString().length() != 9) {
                Datos.this.mostrar(v);
                return;
            }
            Datos.this.dni = Datos.this.dnia.getText().toString();
            Datos.this.requestQueue.add(new StringRequest(1, Datos.URL, new C01351(), new C01362()) {
                public Map<String, String> getParams() throws AuthFailureError {
                    Map<String, String> parameters = new HashMap();
                    parameters.put("nombre", Datos.this.nombrea.getText().toString());
                    parameters.put("dni", Datos.this.dni);
                    parameters.put("direccion", Datos.this.direcciona.getText().toString());
                    parameters.put("correo", Datos.this.correoa.getText().toString());
                    return parameters;
                }
            });
            Datos.this.lanzar(v);
            Datos.this.finish();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0102R.layout.datos);
        this.nombrea = (EditText) findViewById(C0102R.id.etcantidad);
        this.dnia = (EditText) findViewById(C0102R.id.dni);
        this.direcciona = (EditText) findViewById(C0102R.id.direccion);
        this.correoa = (EditText) findViewById(C0102R.id.correo1);
        this.cost = (TextView) findViewById(C0102R.id.textView1);
        this.envio = (Button) findViewById(C0102R.id.button12);
        this.requestQueue = Volley.newRequestQueue(getApplicationContext());
        this.f0b = getIntent().getExtras();
        this.dinero = this.f0b.getString("costo");
        this.envio.setOnClickListener(new C00971());
    }

    public void lanzar(View view) {
        Intent i = new Intent(this, Paso1.class);
        i.putExtra("enviard", this.dni);
        startActivity(i);
    }

    public void mostrar(View v) {
        Toast.makeText(this, "LLenar todos los campos, recuerde el celular son 9 digitos", 0).show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(C0102R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0102R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
