package com.botellevasaqp.botellevas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class Paso2 extends Activity {
    Bundle f2b;
    String bebida;
    private String[] cerveza = new String[]{"Cusqueña", "Arequipeña", "Pilsen", "Brahma", "Corona", "Heineken"};
    private String[] cervezap = new String[]{"S/5.00", "S/6.00", "S/7.00", "S/4.00", "S/4.50", "S/4.20"};
    private String[] cervezapri = new String[]{"5", "6", "7", "4", "4.5", "4.2"};
    private String[] cremas = new String[]{"Beilys", "Crema de Pisco", "Crema de Ron Capricho"};
    private String[] cremasp = new String[]{"s/20.00", "s/16.00", "s/17.00"};
    private String[] cremaspri = new String[]{"20", "16", "17"};
    String dni;
    private String[] espumante = new String[]{"Queirolo", "Tabernero", "Valdivieso"};
    private String[] espumantep = new String[]{"s/30.00", "s/40.00", "s/20.00"};
    private String[] espumantepri = new String[]{"30", "40", "20"};
    private String[] ginger = new String[]{"Paramonga"};
    private String[] gingerp = new String[]{"s/40.00"};
    private String[] gingerpri = new String[]{"40"};
    private String[] licores = new String[]{"Hipnotic"};
    private String[] licoresp = new String[]{"s/24.00"};
    private String[] licorespri = new String[]{"24"};
    private ListView lv1;
    private ListView lv2;
    String f3p;
    private String[] pisco = new String[]{"Santiago Queirolo", "Majes Tradicion", "4 Gallos"};
    private String[] piscop = new String[]{"s/18.00", "s/24.00", "s/20.00"};
    private String[] piscopri = new String[]{"18", "24", "20"};
    int posicion1;
    String precio;
    private String[] ron = new String[]{"Cartavio", "Captain Morgan", "Apelton", "Flor de caña", "Bacardi", "Zacapa", "Pomalca"};
    private String[] ronp = new String[]{"s/20.00", "s/35.00", "s/30.00", "s/35.00", "s/30.00", "s/20.00", "s/15.00"};
    private String[] ronpri = new String[]{"20", "35", "30", "35", "30", "20", "15"};
    String trago;
    private String[] vino = new String[]{"Majes", "Señorito de Najar", "Santiago Queirolo", "Navarro Correas", "Santa julia", "Majestad"};
    private String[] vinop = new String[]{"s/25.00", "s/20.00", "s/18.00", "s/25.00", "s/20.00", "s/45.00"};
    private String[] vinopri = new String[]{"25", "20", "18", "25", "20", "45"};
    private String[] vodka = new String[]{"Russkaya", "Absolut", "Danzka"};
    private String[] vodkap = new String[]{"s/20.00", "s/25.00", "s/30.00"};
    private String[] vodkapri = new String[]{"20", "25", "30"};
    private String[] wisky = new String[]{"Chivas Regal", "Jonnie walker"};
    private String[] wiskyp = new String[]{"s/40.00", "s/45.00"};
    private String[] wiskypri = new String[]{"40", "45"};

    class C01001 implements OnItemClickListener {
        C01001() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int posicion, long arg3) {
            if (Paso2.this.posicion1 == 0) {
                Paso2.this.bebida = Paso2.this.cerveza[posicion];
                Paso2.this.precio = Paso2.this.cervezapri[posicion];
            }
            if (Paso2.this.posicion1 == 1) {
                Paso2.this.bebida = Paso2.this.cremas[posicion];
                Paso2.this.precio = Paso2.this.cremaspri[posicion];
            }
            if (Paso2.this.posicion1 == 2) {
                Paso2.this.bebida = Paso2.this.espumante[posicion];
                Paso2.this.precio = Paso2.this.espumantepri[posicion];
            }
            if (Paso2.this.posicion1 == 3) {
                Paso2.this.bebida = Paso2.this.ginger[posicion];
                Paso2.this.precio = Paso2.this.gingerpri[posicion];
            }
            if (Paso2.this.posicion1 == 4) {
                Paso2.this.bebida = Paso2.this.licores[posicion];
                Paso2.this.precio = Paso2.this.licorespri[posicion];
            }
            if (Paso2.this.posicion1 == 5) {
                Paso2.this.bebida = Paso2.this.pisco[posicion];
                Paso2.this.precio = Paso2.this.piscopri[posicion];
            }
            if (Paso2.this.posicion1 == 6) {
                Paso2.this.bebida = Paso2.this.ron[posicion];
                Paso2.this.precio = Paso2.this.ronpri[posicion];
            }
            if (Paso2.this.posicion1 == 7) {
                Paso2.this.bebida = Paso2.this.vino[posicion];
                Paso2.this.precio = Paso2.this.vinopri[posicion];
            }
            if (Paso2.this.posicion1 == 8) {
                Paso2.this.bebida = Paso2.this.vodka[posicion];
                Paso2.this.precio = Paso2.this.vodkapri[posicion];
            }
            if (Paso2.this.posicion1 == 9) {
                Paso2.this.bebida = Paso2.this.wisky[posicion];
                Paso2.this.precio = Paso2.this.wiskypri[posicion];
            }
            Paso2.this.lanzar(v);
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0102R.layout.paso2);
        this.f2b = getIntent().getExtras();
        this.f3p = this.f2b.getString("posi");
        this.dni = this.f2b.getString("dni2");
        this.trago = this.f2b.getString("bebida");
        this.posicion1 = Integer.parseInt(this.f3p);
        this.lv1 = (ListView) findViewById(C0102R.id.lvstock);
        this.lv2 = (ListView) findViewById(C0102R.id.lvlprecio);
        if (this.posicion1 == 0) {
            this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.cerveza));
            this.lv2.setAdapter(new ArrayAdapter(this, 17367043, this.cervezap));
        }
        if (this.posicion1 == 1) {
            this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.cremas));
            this.lv2.setAdapter(new ArrayAdapter(this, 17367043, this.cremasp));
        }
        if (this.posicion1 == 2) {
            this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.espumante));
            this.lv2.setAdapter(new ArrayAdapter(this, 17367043, this.espumantep));
        }
        if (this.posicion1 == 3) {
            this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.ginger));
            this.lv2.setAdapter(new ArrayAdapter(this, 17367043, this.gingerp));
        }
        if (this.posicion1 == 4) {
            this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.licores));
            this.lv2.setAdapter(new ArrayAdapter(this, 17367043, this.licoresp));
        }
        if (this.posicion1 == 5) {
            this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.pisco));
            this.lv2.setAdapter(new ArrayAdapter(this, 17367043, this.piscop));
        }
        if (this.posicion1 == 6) {
            this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.ron));
            this.lv2.setAdapter(new ArrayAdapter(this, 17367043, this.ronp));
        }
        if (this.posicion1 == 7) {
            this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.vino));
            this.lv2.setAdapter(new ArrayAdapter(this, 17367043, this.vinop));
        }
        if (this.posicion1 == 8) {
            this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.vodka));
            this.lv2.setAdapter(new ArrayAdapter(this, 17367043, this.vodkap));
        }
        if (this.posicion1 == 9) {
            this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.wisky));
            this.lv2.setAdapter(new ArrayAdapter(this, 17367043, this.wiskyp));
        }
        this.lv1.setOnItemClickListener(new C01001());
    }

    public void lanzar(View view) {
        Intent i = new Intent(this, Paso3.class);
        i.putExtra("bebid", this.bebida);
        i.putExtra("preciobe", this.precio);
        i.putExtra("dni3", this.dni);
        i.putExtra("tipobebida", this.trago);
        startActivity(i);
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(C0102R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0102R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
