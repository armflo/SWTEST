package com.botellevasaqp.botellevas;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {
    private String[] costos = new String[]{"10", "11", "12", "13", "14"};
    private String[] distritos = new String[]{"Cayma", "Cercado", "JLBR", "Hunter", "Paucarpata"};
    private ListView lv1;
    String precioenvio = "";

    class C00981 implements OnItemClickListener {
        C00981() {
        }

        public void onItemClick(AdapterView<?> adapterView, View v, int posicion, long arg3) {
            MainActivity.this.precioenvio = MainActivity.this.costos[posicion];
            MainActivity.this.mostrar(v);
            MainActivity.this.lanzar(v);
            MainActivity.this.finish();
        }
    }

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(C0102R.layout.activity_main);
        this.lv1 = (ListView) findViewById(C0102R.id.lv1);
        this.lv1.setAdapter(new ArrayAdapter(this, 17367043, this.distritos));
        this.lv1.setOnItemClickListener(new C00981());
    }

    public void lanzar(View view) {
        Intent i = new Intent(this, Datos.class);
        i.putExtra("costo", this.precioenvio);
        startActivity(i);
    }

    public void mostrar(View v) {
        Toast.makeText(this, "Recuerde estar conectado a internet para enviar el pedido", 0).show();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(C0102R.menu.main, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == C0102R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
